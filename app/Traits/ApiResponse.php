<?php


namespace App\Traits;


trait ApiResponse
{
    public function ans(bool $success, int $code, string $msg, array $content)
    {
        return response()->json([
            'success' => $success,
            'code'    => $code,
            'message' => $msg,
            'content' => $content
        ]);
    }
}
