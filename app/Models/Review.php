<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property int $to_user_id
 * @property int $from_user_id
 * @property string $text
 * @property int $rate
 * @property bool $show
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Answer|null $answer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Complaint[] $complaints
 * @property-read int|null $complaints_count
 * @property-read \App\Models\User|null $fromUser
 * @property-read \App\Models\User|null $toUser
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereFromUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereToUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @property string $reply
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereReply($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUserId($value)
 */
class Review extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function fromUser()
    {
        return $this->hasOne(User::class, 'id', 'from_user_id');
    }

    public function toUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }

    public function complaints()
    {
        return $this->morphMany(Complaint::class, 'model')->where('status', '>=', 0);
    }

    public function answer()
    {
        return $this->hasOne(Answer::class, 'review_id', 'id');
    }
}
