<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Complaint
 *
 * @property int $id
 * @property string $model_type
 * @property int $model_id
 * @property int $user_id
 * @property string $body
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $fromUser
 * @property-read \App\Models\User|null $toUser
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint query()
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereModelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereModelType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereUserId($value)
 * @mixin \Eloquent
 * @property int $review_id
 * @property string $text
 * @property int $closed
 * @property-read Model|\Eloquent $related
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereClosed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereReviewId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Complaint whereText($value)
 */
class Complaint extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function fromUser()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function toUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }

    public function related()
    {
        return $this->morphTo('model','model_type','model_id', 'id');
    }
}
