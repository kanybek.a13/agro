<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserInfo
 *
 * @property int $id
 * @property string $branch
 * @property string $date_of_birth
 * @property string $region
 * @property string $regalia
 * @property string $place_of_work
 * @property string $position
 * @property string $achieved_results
 * @property string $research_topics
 * @property string $cv
 * @property string $photo
 * @property string $contacts
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereAchievedResults($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereDateOfBirth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePlaceOfWork($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereRegalia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereResearchTopics($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $organization
 * @property string|null $field
 * @property string|null $hashtag
 * @property string|null $clients
 * @property string|null $services
 * @property string|null $projects
 * @property string|null $researches
 * @property string|null $phone
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereClients($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereHashtag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereOrganization($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereProjects($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereResearches($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereServices($value)
 */
class UserInfo extends Model
{
    use HasFactory;

    protected $table = 'user_info';

    protected $guarded = [];
}
