<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Organization
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Organization query()
 * @mixin \Eloquent
 */
class Organization extends Model
{
    use HasFactory;

    public static function search($search = '')
    {
        if($search)
            return self::where('name', 'like', '%'.$search.'%')->select('id', 'name')->get();

        return self::select('id', 'name')->get();
    }
}
