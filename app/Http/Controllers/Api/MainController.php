<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Hashtag;
use App\Models\Organization;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    use ApiResponse;

    public function organizations(Request $request)
    {
        $organizations = Organization::search($request->search);

        return $this->ans(true, 200, '', $organizations->toArray());
    }

    public function hashtags(Request $request)
    {
        $validator = Validator::make($request->all(), ['search' => 'required']);
        if($validator->fails()) {
            return $this->ans(false, 400, 'Validation error', $validator->errors()->toArray());
        }

        $hashtags = Hashtag::search($request->search);

        if(!$hashtags->count()) {
            $hashtags = Hashtag::create(['name' => $request->search]);
        }

        return $this->ans(true, 200, '', $hashtags->toArray());
    }

    public function favorite(User $user)
    {
        $curUser = Auth::user();

        if($curUser->isFavorite($user->id))
            return $this->ans(false, '400', 'This user already in your favorites', []);

        $curUser->favorite($user->id);

        return $this->ans(true, '200', 'User is favorite', []);
    }

    public function unfavorite(User $user)
    {
        $curUser = Auth::user();

        if(!$curUser->isFavorite($user->id))
            return $this->ans(false, '400', 'This user dont your favorite', []);

        $curUser->unfavorite($user->id);

        return $this->ans(true, '200', 'User isn\'t your favorite', []);
    }

    public function avatar(Request $request)
    {
        $validator = Validator::make($request->all(), ['file' => 'required|file|image|max:2048']);

        if($validator->fails())
            return $this->ans(false, 400, 'Error file validation', $validator->errors()->toArray());

        $user = Auth::user();

        if($fileName = Storage::disk('content')->put('/',$request->file('file'))) {
            $user->info->forceFill(['avatar' => 'images/user/avatar/'.$fileName])->save();
            return $this->ans(true, 200, 'Avatar uploaded success', []);
        }

        return $this->ans(false, 500, 'When file was uploading error occurred', []);

    }
}
