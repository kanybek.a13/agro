<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function show(){
        $user = auth()->user();

        return view('admin.profile.show', compact('user'));
    }

    public function edit(){
        $user = auth()->user();

        return view('admin.profile.edit', compact('user'));
    }

    public function update(Request $request){
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $user = auth()->user();

        if (\request('old_password') && Hash::check($request->old_password, $user->password)){
            $this->validate($request, [
                'old_password' => 'required',
                'new_password' => 'confirmed|different:password',
            ]);

            $user->update([
                'password' => Hash::make($request->new_password)
            ]);
        }

        $user->update([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
        ]);

        return redirect(route('profile.show'));
    }
}
