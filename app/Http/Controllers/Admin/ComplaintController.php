<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaint;
use Illuminate\Http\Request;

class ComplaintController extends Controller
{
    public function index(Request $request)
    {
        if ($request->status && $request->status != 'all'){
            $complaints = Complaint::where('status', $request->status)->with('related')->get();
        }else
            $complaints = Complaint::with('related')->get();

        return view('admin.complaint.index', compact('complaints'));
    }
}
