<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\Complaint;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index()
    {
        $reviews = Review::with('fromUser', 'toUser')->get();

        return view('admin.review.index', compact('reviews'));
    }

    public function show(Review $review)
    {
        $review->load('fromUser', 'toUser', 'complaints', 'answer', 'answer.complaints');

        return view('admin.review.show', compact('review'));
    }

    public function edit(Review $review)
    {

        return view('admin.review.edit');
    }
//    public function edit(Review $review)
//    {
//        $review->load('toUser', 'fromUser');
//
//        return view('admin.review.edit', compact('review'));
//    }
//
    public function decline(Request $request, Complaint $complaint)
    {
        //Validate
        $request->validate(['body' => 'required']);

        //Send email
        $this->dispatch(new SendEmail($complaint->fromUser->email, $request->body));

        //Update complaint fields
        $complaint->update(['status' => -1]);

        return redirect()->back();
    }

    public function accept(Request $request, Complaint $complaint)
    {
        //Validate
        $request->validate(['body' => 'required']);

        //Load relations
        //$complaint->load('fromUser', 'review.toUser');

        //Send email
        $this->dispatch(new SendEmail($complaint->fromUser->email, 'Ваша жалоба на отзыв была одобрена'));
        //$this->dispatch(new SendEmail($complaint->related->toUser->email, $request->body));

        //Update complaint fields and review
        $complaint->update(['status' => 1]);
        //$complaint->review->update(['show' => false]);

        return redirect()->back();
    }

    public function destroy(Request $request, Review $review) {
        $review->forceDelete();

        return redirect()->back()->with('success', 'Успешно удален');
    }
}
