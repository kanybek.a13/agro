<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ScientistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scientists = User::where('isConsultant', 1)->paginate(10);

        return view('admin.scientist.index', compact('scientists'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if(!$user->isConsultant){
            abort(404);
        }

        $reviews = $user->reviews;

        return view('admin.scientist.show', [
            'scientist' => $user,
            'reviews' => $reviews
        ]);
    }
}
