<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/organizations', [\App\Http\Controllers\Api\MainController::class, 'organizations']);
Route::get('/hastags', [\App\Http\Controllers\Api\MainController::class, 'hashtags']);

Route::post('/favorite/{user}', [\App\Http\Controllers\Api\MainController::class, 'favorite']);
Route::delete('/favorite/{user}', [\App\Http\Controllers\Api\MainController::class, 'unfavorite']);

Route::post('/avatar', [\App\Http\Controllers\Api\MainController::class, 'avatar']);
