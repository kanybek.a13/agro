<?php

use App\Http\Controllers\Admin\ComplaintController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Admin\ScientistController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group( ["prefix" => "panel" ], function() {
    Route::get('', [HomeController::class, 'adminIndex'])->name('admin.admin.index');

    Route::get('login', [LoginController::class, 'showLoginForm'])->name('admin.login.show');
    Route::post('login', [LoginController::class, 'login'])->name('admin.login');

    Route::group( ["prefix" => "profile" ], function() {
        Route::get('/show', [ProfileController::class, 'show'])->name('profile.show');
        Route::get('/edit', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::put('/edit', [ProfileController::class, 'update'])->name('profile.update');

    });

    Route::group(["prefix" => "user_management"], function (){
        Route::resource('roles', RoleController::class);
        Route::resource('users', UserController::class);
        Route::post('users/{id}/moderate', [UserController::class, 'moderate'])->name('users.moderate');
    });

    Route::resource('reviews', ReviewController::class)->only('index', 'show', 'edit', 'destroy');
    Route::post('/review/{complaint}/decline', [ReviewController::class, 'decline'])->name('review.decline');
    Route::post('/review/{complaint}/accept', [ReviewController::class, 'accept'])->name('review.accept');

    Route::get('scientists', [ScientistController::class, 'index'])->name('scientists.index');
    Route::get('scientists/{id}/reviews', [ScientistController::class, 'show'])->name('scientists.show');

    Route::get('complaint', [ComplaintController::class, 'index'])->name('complaints.index');

});

Route::group(['prefix' => 'ajax'], function($router) {



});

