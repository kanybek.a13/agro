<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('organization')->default(null)->nullable();
            $table->string('position')->default(null)->nullable();
            $table->string('field')->default(null)->nullable();
            $table->string('hashtag')->default(null)->nullable();
            $table->text('clients')->default(null)->nullable();
            $table->text('services')->default(null)->nullable();
            $table->text('projects')->default(null)->nullable();
            $table->text('researches')->default(null)->nullable();
            $table->text('phone')->default(null)->nullable();
            $table->string('avatar')->default(null)->nullable();

            $table->unsignedBigInteger('user_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('favorites', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('favorite_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
        Schema::dropIfExists('favorites');
    }
}
