<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_admin = User::create([
            'name' => 'User Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456789')
        ]);

        $role_admin = Role::create(['name' => 'Admin']);

        $permissions = Permission::pluck('id','id')->all();

        $role_admin->syncPermissions($permissions);

        $user_admin->assignRole([$role_admin->id]);

        $user_moderator = User::create([
            'name' => 'User Moderator',
            'email' => 'moderator@gmail.com',
            'password' => bcrypt('123456789')
        ]);

        $role_moderator = Role::create(['name' => 'Moderator']);

        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'permission-list',
            'permission-create',
            'permission-edit',
            'user-list',
            'user-create',
            'user-edit',
        ];

        $role_moderator->givePermissionTo($permissions);

        $user_moderator->assignRole([$role_moderator->id]);

        for ($i = 0; $i < 2; $i++){

             $user = User::create([
                 'name' => 'Sample ' . $i,
                 'email' => 'sample' . $i . '@gmail.com',
                 'password' => bcrypt('123456789'),
                 'isModerated' => 0,
                 'isConsultant' => true,
             ]);

             $user->info()->create([]);

        }



//        //$user->givePermissionTo(['edit articles', 'delete articles']);
//        $store_update_permission = Permission::create(['name' => 'store_update']);
//        $view_permission = Permission::create(['name' => 'view']);
//        $delete_permission = Permission::create(['name' => 'delete']);
//
//        $admin_role = Role::create(['name' => 'admin']);
//        $moderator_role = Role::create(['name' => 'moderator']);
//
//        $admin_role->givePermissionTo($store_update_permission);
//        $admin_role->givePermissionTo($view_permission);
//        $admin_role->givePermissionTo($delete_permission);
//
//        $moderator_role->givePermissionTo($store_update_permission);
//        $moderator_role->givePermissionTo($view_permission);
//
//        $admin = User::create([
//            'name' => 'Admin',
//            'email' => 'admin@gmail.com',
//            'password' => Hash::make('admin'),
//        ]);
//
//        $moderator = User::create([
//            'name' => 'Moderator',
//            'email' => 'moderator@gmail.com',
//            'password' => Hash::make('moderator'),
//        ]);
//
//        $admin->assignRole($admin_role);
//        $moderator->assignRole($moderator_role);
    }
}
