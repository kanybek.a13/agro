<?php

namespace Database\Seeders;

use App\Models\Complaint;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ReviewSeeder extends Seeder
{
    public $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersId = User::select('id')->pluck('id');

        foreach(range(1, 10) as $i) {
            $rand = mt_rand(0, count($usersId) - 1);
            $randUser = ($rand - 1) >= 0 ? $rand - 1 : $rand + 1;
            $review = Review::create([
                'to_user_id' => $usersId[$rand],
                'from_user_id' => $usersId[$randUser],
                'text' => $this->faker->paragraph,
                'rate' => mt_rand(1, 5),
                //'reply' => $this->faker->paragraph
            ]);


            for($i = 0; $i < 2; $i++) {
                $review->complaints()->create([
                    'user_id' => $usersId[$randUser],
                    'body' => $this->faker->paragraph,
                ]);
            }
        }
    }
}
