<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Review;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class AnswerSeeder extends Seeder
{
    public $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersId = User::select('id')->pluck('id');
        $reviewsID = Review::select('id')->pluck('id');

        foreach(range(1, 10) as $i) {
            $randU = mt_rand(0, count($usersId) - 1);
            $randR = mt_rand(0, count($reviewsID) - 1);
            $answer = Answer::create([
                'review_id' => $reviewsID[$randR],
                'from_user_id' => $usersId[$randU],
                'text' => $this->faker->paragraph,
            ]);


            for($i = 0; $i < 2; $i++) {
                $answer->complaints()->create([
                    'user_id' => $usersId[$randU],
                    'body' => $this->faker->paragraph,
                ]);
            }
        }
    }
}
