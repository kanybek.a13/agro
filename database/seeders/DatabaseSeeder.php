<?php

namespace Database\Seeders;

use App\Models\Complaint;
use App\Models\Hashtag;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            UserSeeder::class,
            ReviewSeeder::class,
            AnswerSeeder::class,
            OrganizationSeeder::class,
            HashtagSeeder::class
        ]);
    }
}
