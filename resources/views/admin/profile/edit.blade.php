@include('admin.layouts.header')

<div class="container container-fluid">
    <br>
    <ul class="breadcrumbs">
        <li><a href="{{route('profile.show')}}" title="Профиль">Профиль</a></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <form class="block" method="post" action="{{route('profile.update')}}">
        @csrf
        @method('put')

        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основные реквизиты</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основные данные</a></li>
                        <li><a href="javascript:;" title="Служебная информация">Безопасность</a></li>
                        <li><a href="javascript:;" title="История изменения">История изменения</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular" required>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular" required>
                    </div>
                </div>
                <div>
                    <div class="input-group">
                        <label class="input-group__title"> Текущий пароль</label>
                        <input type="password" name="old_password" class="input-regular" placeholder="Введите пароль" data-validate="password">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Новый пароль</label>
                        <input type="password" name="new_password" class="input-regular" placeholder="Введите пароль" data-validate="password">
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Подтвердите Пароль</label>
                        <input type="password" name="new_password_confirmation" class="input-regular" placeholder="Подтвердите пароль" data-validate="password_confirmation">
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="buttons">
            <div>
                <button type="submit" class="btn btn--green">Сохранить</button>
            </div>
        </div>
    </form>
</div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
