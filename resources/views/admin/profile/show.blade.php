@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><span>Профиль</span></li>
        <li><span>{{$user->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$user->id}}</div>
            <h1 class="fund-header__title">{{$user->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Date of creation</div>
                <div class="property__text">{{$user->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Date of change</div>
                <div class="property__text">{{$user->updated_at}}</div>
            </div>
        </div>
    </div>

    <div class="block">
        <div class="tabs">
            <div class="mobile-dropdown">
                <div class="mobile-dropdown__title dynamic">Основная информация</div>
                <div class="mobile-dropdown__desc">
                    <ul class="tabs-titles">
                        <li class="active"><a href="javascript:;" title="Основные реквизиты">Основная информация</a></li>
                    </ul>
                </div>
            </div>
            <div class="tabs-contents">
                <div class="active">
                    <div class="input-group">
                        <div class="row row--multiline">
                            <div class="col-md-4 col-sm-6">
                                <a href="{{route('profile.edit')}}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit" align="right" ></a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> ФИО</label>
                        <input type="text" name="name" value="{{$user->name}}" placeholder="ФИО" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Почта</label>
                        <input type="email" name="email" value="{{$user->email}}" placeholder="Почта" class="input-regular" disabled>
                    </div>
                    <br>
                    <div class="input-group">
                        <label class="input-group__title"> Role</label>
                        <input type="text" name="role" value="{{$user->roles[0]->name}}" placeholder="Role" class="input-regular" disabled>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
