    <aside class="sidebar">
    <div class="sidebar__top hidden-sm hidden-xs">
        <a href="/" title="Главная" class="logo"><img src="/admin/img/admin-panel-logo.svg" alt=""></a>
    </div>
    <div class="menu-wrapper">
        <ul class="menu">
            <li class="dropdown">
                <a href="javascript:;" title="Index"><i class="icon-organizations"></i>Index</a>
                <ul>
                    <li><a href="/panel" title="Список заявок">Admin index</a></li>
                </ul>
                <ul>
                    <li><a href="/" title="Список заявок">App index</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Мой Профиль"><i class="icon-directory"></i>Мой Профиль</a>
                <ul>
                    <li><a href="{{route('profile.show')}}" title="Посмотреть">Посмотреть</a></li>
                    <li><a href="{{route('profile.edit')}}" title="Редактировать">Редактировать</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Архив"><i class="icon-users"></i>User Management</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>
                    <li><a class="nav-link" href="{{ route('roles.index') }}">Manage Role</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:;" title="Reviews"><i class="icon-users"></i>Reviews</a>
                <ul>
                    <li><a class="nav-link" href="{{ route('reviews.index') }}">List Reviews</a></li>
                    <li><a class="nav-link" href="{{ route('scientists.index') }}">List of Scientist</a></li>
                    <li><a class="nav-link" href="{{ route('complaints.index') }}">List of Complaints</a></li>
                </ul>
            </li>
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="Пользователи"><i class="icon-archive"></i> Пользователи</a>--}}
{{--                <ul>--}}
{{--                    <li><a href="{{route('users.index')}}" title="Список пользователей">Список пользователей</a></li>--}}
{{--                    <li><a href="{{route('users.create')}}" title="Добавить" class="add">+Добавить</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="dropdown">--}}
{{--                <a href="javascript:;" title="Страницы"><i class="icon-archive"></i> Страницы</a>--}}
{{--                <ul>--}}
{{--                    <li><a href="{{route('users.index')}}" title="Список страниц">Список страниц</a></li>--}}
{{--                    <li><a href="{{route('users.create')}}" title="Добавить" class="add">+Добавить</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}

        </ul>
    </div>
</aside>
