@include('admin.layouts.header')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline align-items-center">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">Отзывы</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">
                <div class="flex-form">
                    <div>
                        <form class="input-button">
                            <input type="text" name="search" placeholder="Наименование фонда/Номер фонда" class="input-regular input-regular--solid" style="width: 282px;">
                            <button class="btn btn--green">Найти</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">Список записей</h2>
        <table class="table records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 15%;">
                <col span="1" style="width: 15%;">
                <col span="1" style="width: 15%;">
            </colgroup>
            <thead>
            <tr>
                <th>#</th>
                <th>От кого</th>
                <th>Кому</th>
                <th>Дата создания</th>
                <th>Дата изменения</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reviews as $model)
                <tr>
                    <td>{{ $model->id }}</td>
                    <td>{{ $model->fromUser->name }}</td>
                    <td>{{ $model->toUser->name }}</td>
                    <td>{{ $model->created_at }}</td>
                    <td>{{ $model->updated_at }}</td>
                    <td>
                        <div class="action-buttons">
                            <a href="{{ route('reviews.show', $model->id) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="{{ route('reviews.edit', $model->id) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="javascript:;" title="Удалить" onclick="document.querySelector('#model-{{ $model->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                            <form action="{{ route('reviews.destroy', $model->id) }}" id="model-{{ $model->id }}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-right">
            <ul class="pagination">
                <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="dots disabled"><span>...</span></li>
                <li><a href="#">498</a></li>
                <li><a href="#">499</a></li>
                <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
