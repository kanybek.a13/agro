@include('admin.layouts.header')

<style>
    .complaints {
        display: flex;
        flex-direction: column;
    }

    .complaint {
        width: 60%;
        border-radius: 8px;
        border: 1px solid silver;
        padding: 15px;
        align-self: flex-end;
        background: white;
    }

    .text-right {text-align: right;}
    .mt-10 {margin-top: 10px;}
    .window {display: none; width: 40%}
</style>

<div style="padding: 10px">

    <div class="complaint">

        <h3>
            Review to {{ $review->toUser->name }} on {{ $review->created_at->format('d-m-Y h:m') }}
        </h3>
        <p>
            {{ $review->text }}
        </p>

    </div>

    <div class="complaints mt-10">
        @foreach($review->complaints as $complaint)
            <div class="complaint mt-10">
                @if($complaint->status == 1)
                    ОДОБРЕНО!<br>
                @else
                    НА РАСМОТРЕНИИ!<br>
                @endif
                {{ $complaint->fromUser->name }}<br>
                {{ $complaint->body }}
                <div class="text-right mt-10">
                    <a href="javascript:;" data-fancybox data-src="#decline" onclick="changeUrl('{{ route('review.decline', $complaint->id) }}', 'decline')" class="btn btn--red">Отклонить</a>
                    <a href="javascript:;" data-fancybox data-src="#accept" onclick="changeUrl('{{ route('review.accept', $complaint->id) }}', 'accept')" class="btn btn--green">Одобрить</a>
                </div>
            </div>
        @endforeach
    </div>

    @if($review->answer)
        <div class="complaint mt-10">

            <h3>
                Answer to review
            </h3>

            <p>
                {{ $review->answer->text }}
            </p>

        </div>

        <div class="complaints mt-10">
            @foreach($review->answer->complaints as $complaint)
                <div class="complaint mt-10">
                    @if($complaint->status == 1)
                        ОДОБРЕНО!<br>
                    @else
                        НА РАСМОТРЕНИИ!<br>
                    @endif
                    {{ $complaint->fromUser->name }}<br>
                    {{ $complaint->body }}
                    <div class="text-right mt-10">
                        <a href="javascript:;" data-fancybox data-src="#decline" onclick="changeUrl('{{ route('review.decline', $complaint->id) }}', 'decline')" class="btn btn--red">Отклонить</a>
                        <a href="javascript:;" data-fancybox data-src="#accept" onclick="changeUrl('{{ route('review.accept', $complaint->id) }}', 'accept')" class="btn btn--green">Одобрить</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

</div>

<div id="decline" class="window">
    <form name="decline" method="POST">
        @csrf
        <div class="input-group">
            <label class="input-group__title">Why declined???</label>
            <textarea name="body" class="input-regular"></textarea>
        </div>

        <input type="submit" value="Send" class="btn btn--green">
    </form>
</div>

<div id="accept" class="window">
    <form name="accept" method="POST">
        @csrf
        <div class="input-group">
            <label class="input-group__title">Why accepted???</label>
            <textarea name="body" class="input-regular"></textarea>
        </div>

        <input type="submit" value="Send" class="btn btn--green">
    </form>
</div>

<script>
    function changeUrl(url, formName) {
        let form = document.querySelector('form[name='+ formName +']');
            form.setAttribute('action', url);
    }
</script>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
