@include('admin.layouts.header')

<div class="container container-fluid">
    <ul class="breadcrumbs">
        <li><a href="{{route('scientists.index')}}" title="Scientists">Scientists</a></li>
        <li><span>{{$scientist->name}}</span></li>
    </ul>

    <div class="fund-header">
        <div class="fund-header__left">
            <div class="fund-header__id">#{{$scientist->id}}</div>
            <h1 class="fund-header__title">{{$scientist->name}}</h1>
        </div>
        <div class="fund-header__right">
            <div class="property">
                <div class="property__title">Date of creation</div>
                <div class="property__text">{{$scientist->created_at}}</div>
            </div>
            <div class="property">
                <div class="property__title">Date of change</div>
                <div class="property__text">{{$scientist->updated_at}}</div>
            </div>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">List of Reviews</h2>
        <table class="table records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 10%;">
                <col span="1" style="width: 5%;">
                <col span="1" style="width: 25%;">
                <col span="1" style="width: 15%;">
            </colgroup>
            <thead>
            <tr>
                <th>#</th>
                <th>From</th>
                <th>To</th>
                <th>Date of creation</th>
                <th>Date of change</th>
                <th>Rate</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reviews as $review)
                <tr>
                    <td>{{ $review->id }}</td>
                    <td>{{ $review->fromUser->name }}</td>
                    <td>{{ $review->toUser->name }}</td>
                    <td>{{ $review->created_at }}</td>
                    <td>{{ $review->updated_at }}</td>
                    <td>{{ $review->rate }}</td>
                    <td>{{ $review->text }}</td>
                    <td>
                        <div class="action-buttons">
                            <a href="{{ route('reviews.show', $review->id) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="{{ route('reviews.edit', $review->id) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="javascript:;" title="Удалить" onclick="document.querySelector('#model-{{ $review->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                            <form action="{{ route('reviews.destroy', $review->id) }}" id="model-{{ $review->id }}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
