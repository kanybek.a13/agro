@include('admin.layouts.header')

<main class="main">
    <div class="container ">
        <section>
            <h2 class="title-primary">Scientists</h2>
            <form action="{{route('scientists.index')}}" method="GET">
                <div class="row row--multiline">
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Name</label>
                            <input type="text" name="name" class="input-regular" placeholder="Name"  value="{{request('name')}}">
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="input-group">
                            <label class="input-group__title">Email</label>
                            <input type="text" name="email" class="input-regular" data-validate="email" placeholder="E-mail" value="{{request('email')}}">
                        </div>
                    </div>

                    <div class="col-md-2 col-sm-6">
                        <label class="input-group__title hidden-xs">&nbsp;</label>
                        <button class="btn full-width" >Apply</button>
                    </div>
                </div>
            </form>
        </section>
    </div>

    <div class="container container-fluid">
        <div class="block">
            <h2 class="title-secondary">List of Scientists</h2>

            <table class="table records">
                <colgroup>
                    <col span="1" style="width: 3%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 20%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                    <col span="1" style="width: 15%;">
                </colgroup>
                <thead>
                <tr>
                    <th>№</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($scientists as $scientist)
                    <tr>
                        <td>{{$scientist->id}}</td>
                        <td>{{$scientist->name}}</td>
                        <td>{{$scientist->email}}</td>
                        <td>
                            <div class="action-buttons">
                                <a href="{{route('scientists.show', $scientist->id)}}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
{{--                                <a href="#" title="Удалить" onclick="document.getElementById('form-delete-{{ $scientist->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>--}}
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <ul class="pagination">
                <li class="previous_page"><a {{$scientists->currentPage() === 1 ? "disabled" : '' }} href="{{route('scientists.index')}}/?page={{ $scientists->currentPage() - 1 }}"><i class="icon-left"></i></a></li>

                @for($page = 1; $page <= $scientists->lastPage(); $page++)
                        <li><a {{ $page === $scientists->currentPage() ? 'class="active"' : '' }} href="{{route('scientists.index')}}/?page={{$page}}">{{ $page }}</a></li>
                @endfor

                <li class="next_page"><a {{$scientists->currentPage() === $scientists->lastPage() ? 'disabled' : '' }} href="{{route('scientists.index')}}/?page={{ $scientists->currentPage() + 1 }}"><i class="icon-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
