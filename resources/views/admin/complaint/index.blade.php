@include('admin.layouts.header')

<div class="container container-fluid">
    <div class="title-block">
        <div class="row row--multiline">
            <div class="col-md-4">
                <h1 class="title-primary" style="margin-bottom: 0">Complaints</h1>
            </div>
            <div class="col-md-8 text-right-md text-right-lg">

                <div class="flex-form center">
                    <form class="input-button" action="{{ route('complaints.index') }}" method="GET">

                        <select name="status" class="chosen no-search input-regular" data-placeholder="All" required>
                            <option class="hidden" value="all" hidden>Все</option>

                            <option value="all" {{request('status') === 'all' ? 'selected' :''}} >All</option>
                            <option value="1" {{request('status') == '1' ? 'selected' :''}} >Approved</option>
                            <option value="0" {{request('status') == '0' ? 'selected' :''}} >Under consideration</option>
                            <option value="-1" {{request('status') == '-1' ? 'selected' :''}} >Rejected</option>
                        </select>

                        <button class="btn btn--green">Apply</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="block">
        <h2 class="title-secondary">List of Complaints</h2>
        <table class="table records">
            <colgroup>
                <col span="1" style="width: 3%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 12%;">
                <col span="1" style="width: 20%;">
                <col span="1" style="width: 15%;">
                <col span="1" style="width: 15%;">
                <col span="1" style="width: 15%;">
            </colgroup>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>From</th>
{{--                    <th>To</th>--}}
                    <th>Status</th>
                    <th>Date of creation</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($complaints as $complaint)
                <tr>
                    <td>{{ $complaint->id }}</td>
                    <td>{{ $complaint->fromUser->name }}</td>
{{--                    <td>{{ $complaint->model->toUser->name }}</td>--}}
                    <td>
                        @if ($complaint->status === 1)
                            Approved
                        @elseif ($complaint->status === -1)
                            Rejected
                        @else
                            Under consideration
                        @endif
                    </td>
                    <td>{{ $complaint->created_at }}</td>
                    <td>
                        <div class="action-buttons">
                            <a href="{{ route('reviews.show', $complaint->id) }}" title="Посмотреть" class="icon-btn icon-btn--green icon-eye"></a>
                            <a href="{{ route('reviews.edit', $complaint->id) }}" title="Редактировать" class="icon-btn icon-btn--yellow icon-edit"></a>
                            <a href="javascript:;" title="Удалить" onclick="document.querySelector('#model-{{ $complaint->id }}').submit()" class="icon-btn icon-btn--pink icon-delete"></a>
                            <form action="{{ route('reviews.destroy', $complaint->id) }}" id="model-{{ $complaint->id }}" method="post">
                                @csrf
                                @method('delete')
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="text-right">
            <ul class="pagination">
                <li class="previous_page disabled"><span><i class="icon-chevron-left"></i></span></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="dots disabled"><span>...</span></li>
                <li><a href="#">498</a></li>
                <li><a href="#">499</a></li>
                <li class="next_page"><a href="#"><i class="icon-chevron-right"></i></a></li>
            </ul>
        </div>
    </div>

@extends('admin.layouts.footer')
@section('content')
    <!--Only this page's scripts-->
    <!---->
@endsection
